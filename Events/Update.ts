import UserCmd from "gitlab.com/FNT_Rework/wrapper/wrapper/Native/UserCmd";
import { Events } from "wrapper/Imports";
import { NET_WORTH_DATA } from "../data";
import { NWValidation } from "../validation";

Events.on("Update", () => {
	if (!NWValidation.IsInGame)
		return
	NET_WORTH_DATA.IsOpenShop = new UserCmd().ShopMask === 13
})
