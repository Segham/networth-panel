import { DOTAGameUIState_t, GameState, Input, InputEventSDK, VMouseKeys } from "wrapper/Imports"
import { NET_WORTH_DATA } from "../data"
import { MenuSettingsTouch } from "../menu"
import { NWValidation } from "../validation"

const IsValidInput = (key: VMouseKeys) => {
	return NWValidation.IsInGame
		&& MenuSettingsTouch.value
		&& key === VMouseKeys.MK_LBUTTON
		&& GameState.UIState === DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME
}

InputEventSDK.on("MouseKeyUp", key => {
	if (!IsValidInput(key))
		return true
	NET_WORTH_DATA.dirtyPosition = false
	return true
})

InputEventSDK.on("MouseKeyDown", key => {
	if (!IsValidInput(key))
		return true
	const Size = NET_WORTH_DATA.HeroSize
	const position = NET_WORTH_DATA.HeroPosition
	const isHoverPanel = Input.CursorOnScreen.IsUnderRectangle(position.x, position.y, Size.x, Size.y)

	if (!isHoverPanel)
		return true

	NET_WORTH_DATA.dirtyPosition = true
	NET_WORTH_DATA.mouseOnPanel.CopyFrom(Input.CursorOnScreen.Subtract(NET_WORTH_DATA.HeroPosition))
	return false
})
