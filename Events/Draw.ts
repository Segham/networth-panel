import { DOTAGameUIState_t, EventsSDK, GameState, Input, LocalPlayer, RendererSDK, Vector2 } from "wrapper/Imports"
import { NET_WORTH_DATA } from "../data"
import { HiddenIfOpenShop, MenuSDK, MenuSettingsTouch, SettingsPosition, SettingsSize, StateAllies, StateEnemies } from "../menu"
import { NWRender } from "../Render"
import { NWValidation } from "../validation"

EventsSDK.on("Draw", () => {
	if (!NWValidation.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return

	const mousePos = Input.CursorOnScreen
	const owner_team = LocalPlayer!.Team
	const textSize = SettingsSize.value * 0.7
	const windowSize = RendererSDK.WindowSize

	const lineSize = new Vector2(SettingsSize.value * 5.5, SettingsSize.value)
	const heroSize = new Vector2(SettingsSize.value * 1.5, SettingsSize.value)
	const hero_position = windowSize.DivideScalar(100).MultiplyForThis(SettingsPosition.Vector)

	if (MenuSettingsTouch.value) {
		NET_WORTH_DATA.HeroSize.CopyFrom(heroSize)
		NET_WORTH_DATA.HeroPosition.CopyFrom(hero_position)

		if (NET_WORTH_DATA.dirtyPosition) {
			MenuSDK.Base.SaveConfigASAP = true
			hero_position.CopyFrom(mousePos.Subtract(NET_WORTH_DATA.mouseOnPanel))
			SettingsPosition.Vector = hero_position.Divide(windowSize).MultiplyScalarForThis(100).Round(1)
		}
	}

	if (NET_WORTH_DATA.IsOpenShop && HiddenIfOpenShop.value)
		return

	NET_WORTH_DATA.RunTimeSortNetWorth.forEach(([unit, data]) => {

		if (unit.Team === owner_team) {
			if (!StateAllies.value)
				return
			NWRender.DrawGradient(hero_position, lineSize)
		} else {
			if (!StateEnemies.value)
				return
			NWRender.DrawGradient(hero_position, lineSize, true)
		}
		NWRender.DrawUnit(unit, hero_position, heroSize)
		NWRender.DrawText(unit, Math.round(data.NetWorth), hero_position.Clone(), lineSize, heroSize, textSize)
		hero_position.AddForThis(new Vector2(0, heroSize.y + 2))
	})
})
