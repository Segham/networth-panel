import { EventsSDK, PlayerResource } from "wrapper/Imports"
import { NET_WORTH_DATA } from "../data"
import { NWValidation } from "../validation"
import { PlayerX } from "../X-Core/Imports"

EventsSDK.on("Tick", () => {
	if (!NWValidation.IsInGame || PlayerResource === undefined)
		return
	NET_WORTH_DATA.RunTimeSortNetWorth = [...PlayerX.DataX.entries()]
		.sort((ahero, bHero) => bHero[1].NetWorth - ahero[1].NetWorth)
})
