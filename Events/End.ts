import { EventsX } from "X-Core/Imports"
import { NET_WORTH_DATA } from "../data"
import { NWRender } from "../Render"

EventsX.on("GameEnded", () => {
	NWRender.Dispose()
	NET_WORTH_DATA.Dispose()
})
