import { GameRules, LocalPlayer, PlayerResource } from "wrapper/Imports"
import { State } from "./menu"

export class NWValidation {
	public static get IsInGame() {
		return State.value
			&& GameRules !== undefined
			&& GameRules.IsInGame
			&& LocalPlayer !== undefined
			&& LocalPlayer.Hero !== undefined
			&& !LocalPlayer.IsSpectator
			&& PlayerResource !== undefined
	}
}
