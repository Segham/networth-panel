import { Color, GameSleeper, TeamData, Unit, Vector2 } from "wrapper/Imports"
import { CPlayerX } from "./X-Core/Imports"

export class NET_WORTH_DATA {

	public static Units: Unit[] = []
	public static IsOpenShop = false
	public static dirtyPosition = false

	public static Sleeper = new GameSleeper()
	public static mouseOnPanel = new Vector2()
	public static TeamData: Nullable<TeamData>
	public static HeroSize = new Vector2(0, 0)
	public static HeroPosition = new Vector2(0, 0)
	public static RunTimeSortNetWorth: [Unit, CPlayerX][] = []

	public static PlColor = new Map<number, Color>()
		.set(0, new Color(51, 117, 255))
		.set(1, new Color(102, 255, 191))
		.set(2, new Color(191, 0, 191))
		.set(3, new Color(243, 240, 11))
		.set(4, new Color(255, 107, 0))
		.set(5, new Color(254, 134, 194))
		.set(6, new Color(161, 180, 71))
		.set(7, new Color(101, 217, 247))
		.set(8, new Color(0, 131, 33))
		.set(9, new Color(164, 105, 0))

	public static Dispose() {
		this.Units = []
		this.IsOpenShop = false
		this.Sleeper.FullReset()
		this.TeamData = undefined
		this.RunTimeSortNetWorth = []
		this.HeroSize = new Vector2()
		this.mouseOnPanel = new Vector2()
		this.HeroPosition = new Vector2()
	}
}
