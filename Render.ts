import { Color, Hero, Input, PlayerResource, RendererSDK, SpiritBear, Unit, Vector2 } from "wrapper/Imports"
import { NET_WORTH_DATA } from "./data"

export class NWRender {

	public static IsRect = false

	public static DrawUnit(unit: Unit, position: Vector2, Size: Vector2) {
		RendererSDK.Image(unit instanceof SpiritBear ? this.Units(unit.Name, true) : this.Units(unit.Name),	position, -1, Size)
		if (!(unit instanceof Hero))
			return
		this.IsRect = Input.CursorOnScreen.IsUnderRectangle(position.x, position.y, Size.x, Size.y)
		const pcolor = NET_WORTH_DATA.PlColor.get(unit.PlayerID) ?? Color.White
		RendererSDK.FilledRect(position.Clone().SubtractScalarX(3), new Vector2(3, Size.y), pcolor.SetA(180))
	}

	public static DrawGradient(position: Vector2, Size: Vector2, IsEnemy: boolean = false) {
		RendererSDK.Image("gitlab.com/FNT_Rework/networth-panel/scripts_files/networth_gradient.svg",
			position, -1,
			Size,
			IsEnemy ? Color.Red.SetA(200) : Color.Green.SetA(200),
		)
	}

	public static DrawText(unit: Unit, net_worth: number, position: Vector2, lineSize: Vector2, heroSize: Vector2, textSize: number) {
		const pos = position.AddForThis(new Vector2(heroSize.x + 5, (lineSize.y - textSize - 2)))
		const text = !this.IsRect ? this.numberWithCommas(net_worth) : this.PlayerName(unit, net_worth)
		RendererSDK.Text(text, pos, Color.White, "Calibri", textSize)
	}

	public static Dispose() {
		this.IsRect = false
	}

	private static readonly DOTAPathHeroesBig = "panorama/images/heroes/"
	private static readonly Bear = NWRender.DOTAPathHeroesBig + "npc_dota_lone_druid_bear"

	private static PlayerName(unit: Unit, net_worth: number) {
		if (!(unit instanceof Hero))
			return net_worth.toString()
		const name = PlayerResource!.PlayerData[unit.PlayerID]?.Name ?? net_worth.toString()
		if (name.length <= 8)
			return name
		return name.slice(0, 7) + "…"
	}

	private static numberWithCommas = (num: number) =>
		num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")

	private static readonly Units = (name: string, isBear: boolean = false) => {
		return !isBear
			? `${NWRender.DOTAPathHeroesBig + name}_png.vtex_c`
			: `${NWRender.Bear}_png.vtex_c`
	}
}
