import { Menu as MenuSDK, Vector2 } from "wrapper/Imports"
const Menu = MenuSDK.AddEntryDeep(["Visual", "Net Worth Panel"])
MenuSDK.Localization.AddLocalizationUnit("russian", new Map([["Net Worth Panel", "Панель нетворса"]])) // TODO tooltip

const State = Menu.AddToggle("State", true, "State script: ON or OFF")
const StateAllies = Menu.AddToggle("Allies", true)
const StateEnemies = Menu.AddToggle("Enemies", true)
const MenuSettings = Menu.AddNode("Settings")
const HiddenIfOpenShop = MenuSettings.AddToggle("Hide if is open shop")
const MenuSettingsTouch = MenuSettings.AddToggle("Touch panel", true, "Move the panel with the mouse (works on the first hero icon!)")
const SettingsSize = MenuSettings.AddSlider("Size", 32, 20, 60)
const SettingsPosition = Menu.AddVector2("Settings", new Vector2(0, 32))

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Allies", "Союзные"],
	["Enemies", "Вражеские"],
	["Touch panel", "Захват панели"],
	["Hide if is open shop", "Скрыть, если открыт магазин"],
	["Move the panel with the mouse (works on the first hero icon!)", "Переместите панель мышью (работает на первую иконку героя!)"],
]))

export {
	State,
	MenuSDK,
	StateAllies,
	StateEnemies,
	SettingsSize,
	MenuSettings,
	HiddenIfOpenShop,
	SettingsPosition,
	MenuSettingsTouch,
}
